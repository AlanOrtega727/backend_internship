const bodyParser = require("body-parser");
const express = require("express");
//api rick and morty
const { getCharacter } = require("./operations/getCharacter");
const { getLocation } = require("./operations/getLocation");
const { getEpisode } = require("./operations/getEpisodes");

const { useraux } = require("./operations/createUser");
const { createUser } = require("./operations/createUser");
const { deleteUser } = require("./operations/DeleteUser");
const {updateUsers} = require("./operations/updateUser");

const PORT = 3000;
const BASE_URL_RM = "/enjoy/api/rm/v1";
const BASE_URL_USERS = "/enjoy/api/users/v1";

// Definicion de app y middelwares
const app = express();
app.use(bodyParser.json());

//  Ruta base
app.get("/", (req, res) => res.send("Server running"));

// Rick & Morty API
app.get(`${BASE_URL_RM}/character/:id`, async (req, res) => {
  const id = req.params?.id ?? 0;

  try {
    const result = (await getCharacter(id)) ?? [];
    res.json(result);
  } catch (err) {
    res.json({
      success: false,
      message: `${err}`,
    });
  }
});

app.get(`${BASE_URL_RM}/episode/:id`, async (req, res) => {
  const id = req.params?.id ?? 0;

  try {
    const result = (await getEpisode(id)) ?? [];
    res.json(result);
  } catch (err) {
    res.json({
      success: false,
      message: `${err}`,
    });
  }
});

app.get(`${BASE_URL_RM}/location/:id`, async (req, res) => {
  const id = req.params?.id ?? 0;

  try {
    const result = (await getLocation(id)) ?? [];
    res.json(result);
  } catch (err) {
    res.json({
      success: false,
      message: `${err}`,
    });
  }
});





// USERS API

app.post(`${BASE_URL_USERS}/create`, async (req, res) => {
  try {
    const result = await createUser(req.body);
    res.json({
      success: true,
      message: result,
    });
    
  } catch (err) {
    res.json({
      success: false,
      message: `${err}`,
    });
  }
});



app.delete(`${BASE_URL_USERS}/deleteUser/:id`, async (req, res) => {
  const id = req.params?.id ?? 0;
  
  try {
    const result = (await deleteUser(id)) ?? [];
    res.json(result);
  } catch (err) {
    res.json({
      success: false,
      message: `${err}`,
    });
  }
});


app.put(`${BASE_URL_USERS}/update`, async (req, res) => {
  
  try {
    const result = await updateUsers(req.body);    
    res.json({
      success: true,
      message: result,
    });
    
  } catch (err) {
    res.json({
      success: false,
      message: `${err}`,
    });
  }
});





app.listen(PORT, () => console.log(`Server Running ${PORT}!`));
