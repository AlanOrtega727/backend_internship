const axios = require("axios");

function getLocation(id = null) {
  return new Promise((resolve, reject) => {
    if (id === null || id === undefined)
      reject(new Error("id null or undefined"));

    axios
      .get(`https://rickandmortyapi.com/api/location/${id}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((err) => {
        reject(`${err}`);
      });
  });
}

module.exports = {
    getLocation: getLocation,
};