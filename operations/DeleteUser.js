const {backUsers} = require("./createUser");
const {updateNewUsers} = require("./createUser");




function deleteUser(id=0){   
    return new Promise((resolve, reject) => {
        if (id === null || id === undefined){
            reject(new Error("The id invalid"));
        }    
    
    try {
        let usersFiltered=[];
        const users=backUsers();
        let userDeleted;
        users.forEach(element => {
            if(element.id!=id){
                usersFiltered.push(element) ;
            }else{
                userDeleted=element;
            }
        });
        console.log(usersFiltered);
        updateNewUsers(usersFiltered);
        
        resolve(`The user ${userDeleted.username} has been deleted` );
    } catch (error) {
        reject(error)
    }
    })
}



module.exports= {
    deleteUser : deleteUser,    
};

