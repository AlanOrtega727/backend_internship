const axios = require("axios");

function getEpisode(id = null) {
  return new Promise((resolve, reject) => {
    if (id === null || id === undefined)
      reject(new Error("id null or undefined"));

    axios
      .get(`https://rickandmortyapi.com/api/episode/${id}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((err) => {
        reject(`${err}`);
      });
  });
}

module.exports = {
    getEpisode: getEpisode,
};
