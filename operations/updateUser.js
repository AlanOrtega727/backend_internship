const {backUsers}= require("./createUser");
const {updateNewUsers}= require("./createUser");


function updateUsers(user={}){
    users=backUsers();
    let index;
        for (let i in users) {
            if(users[i].id==user.id)index=i;
        }
    

    return new Promise((resolve,reject)=>{        
        if(user==null || user==undefined){
            reject (new error("Enter some value to update"));
        }
        if(!user.id){
            reject (new error("id is null or undefinesd"));
        }

        ///////
        if(user.username!=null||user.username!=undefined){
            users[index].username=user.username;    
        }
        if(user.type!=null||user.type!=undefined){
            users[index].type=user.type;
        }
        if(user.permissions!=null||user.permissions!=undefined){
            users[index].permissions=user.permissions;
        }
        updateNewUsers(users);


        resolve(`user updated`);
    });
}

module.exports={
    updateUsers : updateUsers,
}